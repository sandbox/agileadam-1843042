General
============================
I wrote this module as a way to let a client
add available options to a select list. Reworking
the fields to be taxonomy-based was not an option,
unfortunately.

The fields are validated to make sure they key type
is correct. For example, list(integer) fields should
only accept integer key values.

The fields are also validated to make sure the user
doesn't try to add a key that already exists.

Installation / Configuration
============================
1) Enable the module
2) Visit the permissions page and enable
   config/usage permissions
3) Configure module at /admin/config/content/listadmin
4) Use the module at /admin/structure/listadmin
